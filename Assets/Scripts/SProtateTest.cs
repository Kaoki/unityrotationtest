﻿using UnityEngine;
using System.Collections;

public class SProtateTest : MonoBehaviour {
	float xRotation;
	float yRotation;
	float zRotation;
    void Update() {
        xRotation = 0;
        yRotation = -Input.GetAxis("Horizontal");
        zRotation = Input.GetAxis("Vertical");
        transform.Rotate(xRotation, yRotation, zRotation);
    }
}
